package com.book.root.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.book.root.entity.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, String> {

}
