package com.book.root.serviceImpl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.book.root.bean.AddBookRequest;
import com.book.root.bean.AuthorBean;
import com.book.root.bean.BookBean;
import com.book.root.bean.BookResponse;
import com.book.root.entity.Author;
import com.book.root.entity.Book;
import com.book.root.repository.BookRepository;
import com.book.root.service.BookService;
import com.book.root.utils.Constants;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class BookServiceImpl implements BookService {
	
	private static final Logger LOG = LoggerFactory.getLogger(BookServiceImpl.class);

	private final BookRepository bookRepository;

	@Override
	public String addBook(AddBookRequest req) {

		LOG.info(" Entered in BookServiceImpl.addBook()");
		String res = "";
		try {
			List<Author> author = req.getAuthor().stream().map(b -> new Author(b.getName())).collect(Collectors.toList());

			Book book = new Book(req.getName(), req.getCategory(), req.getDiscription(), author, req.getEdition());

			bookRepository.save(book);
			res = "Book added successfully !!!";
		} catch (Exception e) {
			LOG.error("Exception in BookServiceImpl.addBook()");
			res = Constants.ERROR_MSG;
		}
		
		return res;
	}

	@Override
	public BookResponse getBookList() {
		
		LOG.info(" Entered in BookServiceImpl.getBookList()");

		BookResponse res = new BookResponse();
		try {
			List<Book> bookList = bookRepository.findAll();

			List<BookBean> books = bookList.stream()
					.map(b -> new BookBean(b.getId(), b.getName(), b.getCategory(), b.getDiscription(),
							b.getAuthor().stream().map(a -> new AuthorBean(a.getName())).collect(Collectors.toList()),
							b.getEdition()))
					.collect(Collectors.toList());

			res.setBookList(books);
		} catch (Exception e) {
			LOG.error("Exception in BookServiceImpl.getBookList()");
		}
		
		
		return res;
	}

	@Override
	public BookBean getByBookId(String id) {
		
		LOG.info(" Entered in BookServiceImpl.getByBookId()");
		BookBean res = null;
		try {
			Optional<Book> book = bookRepository.findById(id);
			
			if (book.isPresent()) {
				Book b = book.get();
				res = new BookBean(b.getId(), b.getName(), b.getCategory(), b.getDiscription(),
						b.getAuthor().stream().map(a -> new AuthorBean(a.getName())).collect(Collectors.toList()),
						b.getEdition());
			}
			
		} catch (Exception e) {
			LOG.error("Exception in BookServiceImpl.getByBookId()");
		}
		

		return res;
	}

	@Override
	public String deleteByBookId(String id) {
		
		LOG.info(" Entered in BookServiceImpl.deleteByBookId()");
		
		String resMsg = "";
		
		try {
			if(id!=null && !"".equals(id)) {
				bookRepository.deleteById(id);
				resMsg = "Deleted Successfully";
			} else {
				resMsg = "User id is mandatory";
			}
		} catch (Exception e) {
			LOG.error("Exception in BookServiceImpl.deleteByBookId()");
			resMsg = Constants.ERROR_MSG;
		}
		

		return resMsg;
	}

	@Override
	public String updateByBookId(String id, BookBean book) {
		
		LOG.info(" Entered in BookServiceImpl.updateByBookId()");

		String res = "";
		
		try {
			if(id!=null && !"".equals(id)) {
				Optional<Book> b = bookRepository.findById(id);
				if (b.isPresent()) {

					Book bk = b.get();
					bk.setName(book.getName());
					bk.setCategory(book.getCategory());
					bk.setDiscription(book.getDiscription());
					bk.setAuthor(book.getAuthors().stream().map(a -> new Author(a.getName())).collect(Collectors.toList()));
					bk.setEdition(book.getEdition());
					bookRepository.save(bk);
					res = "Book details updated successfully";
				}
			} else {
				res = "User id is mandatory";
			}
		} catch (Exception e) {
			LOG.error("Exception in BookServiceImpl.deleteByBookId()");
			res = Constants.ERROR_MSG;
		}
		
		
		return res;
	}

}
