package com.book.root.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.book.root.bean.AddBookRequest;
import com.book.root.bean.BookBean;
import com.book.root.bean.BookResponse;
import com.book.root.service.BookService;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/book")
@Api(value = "Book Service")
@AllArgsConstructor
public class BookController {

	private static final Logger LOG = LoggerFactory.getLogger(BookController.class);

	private final BookService bookService;

	@PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> addBook(@RequestBody AddBookRequest addBookRequest) {

		LOG.info("**** Entered BookController.addUser() ****");

		String res = bookService.addBook(addBookRequest);

		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BookResponse> getBookList() {

		LOG.info("**** Entered BookController.getBookList() ****");
		BookResponse res = bookService.getBookList();

		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@GetMapping(value = "/{book_ID}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BookResponse> getByBookId(@PathVariable("book_ID") String bookId) {

		LOG.info("**** Entered BookController.getByBookId() ****");
		BookBean userBean = bookService.getByBookId(bookId);
		BookResponse res = new BookResponse();
		List<BookBean> l = new ArrayList<>();
		if (userBean != null) {
			l.add(userBean);
		}
		res.setBookList(l);

		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@DeleteMapping(value = "/{book_ID}")
	public ResponseEntity<String> deleteByBookId(@PathVariable("book_ID") String bookId) {

		LOG.info("**** Entered BookController.deleteByBookId() ****");
		String res = bookService.deleteByBookId(bookId);
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@PutMapping(value = "/{book_ID}")
	public ResponseEntity<String> updateByBookId(@PathVariable("book_ID") String bookId, @RequestBody BookBean book) {

		LOG.info("**** Entered BookController.updateByBookId() ****");
		String res = bookService.updateByBookId(bookId, book);
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

}
