package com.book.root.bean;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BookBean {
	
	private String id;
	private String name;
	private String category;
	private String discription;
	private List<AuthorBean> authors;
	private String edition;
}
