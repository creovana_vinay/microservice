package com.book.root.bean;

import java.util.List;

import lombok.Data;

@Data
public class BookResponse {
	
	private List<BookBean> bookList;

}
