package com.book.root.bean;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddBookRequest {
	
	private String name;
	private String edition;
	private String category;
	private String discription;
	private List<AuthorBean> author;
	

}
