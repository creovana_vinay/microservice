package com.book.root.service;

import com.book.root.bean.AddBookRequest;
import com.book.root.bean.BookBean;
import com.book.root.bean.BookResponse;

public interface BookService {

	public String addBook(AddBookRequest request);

	public BookResponse getBookList();

	public BookBean getByBookId(String id);

	public String deleteByBookId(String id);

	public String updateByBookId(String Id,BookBean book);

}
