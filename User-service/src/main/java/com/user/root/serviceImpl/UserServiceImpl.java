package com.user.root.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.user.root.bean.AddUserRequest;
import com.user.root.bean.UserBean;
import com.user.root.bean.UserResponse;
import com.user.root.entity.User;
import com.user.root.exception.UserNotFoundException;
import com.user.root.repository.UserRepository;
import com.user.root.service.UserService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

	private final UserRepository userRepository;

	@Override
	public String addUser(AddUserRequest request) {

		LOG.info("**** Entered UserServiceImpl.addUser() ****");
		String res = "";
		try {
			User user = new User(request.getUserName(), request.getEmail(), request.getPassword());
			userRepository.save(user);
			res = "User added successfully !";
		} catch (Exception e) {
			LOG.error("Exception in UserServiceImpl.addUser()");
			res = "Sorry unable to process....";
		}

		return res;
	}

	@Override
	public UserResponse getUserList() {

		LOG.info("**** Entered UserServiceImpl.getUserList() ****");

		UserResponse res = new UserResponse();
		try {

			List<User> userList = userRepository.findAll();

			List<UserBean> users = userList.stream().map(u -> new UserBean(u.getUserName(), u.getEmail()))
					.collect(Collectors.toList());
			res.setUserList(users);

		} catch (Exception e) {
			LOG.error("Exception in UserServiceImpl.getUserList()");
		}
		return res;
	}

	@Override
	public UserBean getByUserId(String email) {

		LOG.info("**** Entered UserServiceImpl.getByUserId() ****");

		UserBean res = null;

		try {
			User user = userRepository.findByEmail(email);
			if (user != null) {
				res = new UserBean(user.getUserName(), user.getEmail());
			} else {
				throw new UserNotFoundException("User not found for the Email id = "+email);
			}
		} catch (Exception e) {
			LOG.error("Exception in UserServiceImpl.getByUserId()");
		}
		return res;
	}

	@Override
	public String deleteByUserId(String email) {

		LOG.info("Entered in UserServiceImpl.deleteByUserId()");
		String res = "";
		try {
			userRepository.deleteByEmail(email);
			res = "User deleted  successfully !!";
		} catch (Exception e) {
			LOG.error("Exception in UserServiceImpl.deleteByUserId()");
			res = "Sorry unable to process";
		}

		return res;
	}

	@Override
	public String updateByUserId(String email,UserBean userBean) {
		
		LOG.info("Entered in UserServiceImpl.updateByUserId()");
		String res = "";
		try {
			
			User user = userRepository.findByEmail(email);
			user.setEmail(userBean.getEmail());
			user.setUserName(userBean.getUserName());
			userRepository.save(user);
			
			res = "User updated successfully !!";
		} catch (Exception e) {
			LOG.error("Exception in UserServiceImpl.updateByUserId()");
			res = "Sorry unable to process";
		}

		return res;
	}

}
