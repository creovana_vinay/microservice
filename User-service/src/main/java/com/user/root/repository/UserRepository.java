package com.user.root.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.user.root.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

	public User findByEmail(String eamil);
	
	@Transactional
	@Modifying
	public int deleteByEmail(String email);
	
}
