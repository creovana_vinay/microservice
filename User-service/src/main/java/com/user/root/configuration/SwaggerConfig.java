package com.user.root.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
	public Docket swaggerApi() {
		
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(getApiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.user.root"))
				.paths(PathSelectors.any())
				.build();
	}

	private ApiInfo getApiInfo() {
		return new ApiInfoBuilder()
				.title("User Service")
				.description("User Service")
				.contact(new Contact("EPAM", "https://www.epam.com/", "CS_department@sit.com"))
				.license("Proprietary")
				.licenseUrl("")
				.version("1.0.0")
				.build();
	}
}
