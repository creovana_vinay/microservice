package com.user.root.service;

import com.user.root.bean.AddUserRequest;
import com.user.root.bean.UserBean;
import com.user.root.bean.UserResponse;

public interface UserService {
	
	public String addUser(AddUserRequest request);
	
	public UserResponse getUserList();
	
	public UserBean getByUserId(String email);
	
	public String deleteByUserId(String email);
	
	public String updateByUserId(String eamil, UserBean userBean);

}
